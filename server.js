const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000

const app = express();

// Middleware
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('<h1>Hello World</h1>')
});

// Test
const fs = require('fs');

app.get('/package', (req, res) => {
    if(process.env.NODE_ENV !== 'production') {
        //let pack = fs.readFileSync("package.json");
        let json = require("../DonjonsGestion/package.json")
        let jsonPack = JSON.stringify(json.dependencies);
        let jsonPackDev = JSON.stringify(json.devDependencies);
        res.send(
        `<div>
            <p> Dépendances : ${jsonPack}</p>
            <p> Dépendances Dév : ${jsonPackDev}</p>
        </div>`
        )
    }
    else {
        res.send('404 : Page non trouvée')
    }
});
    
app.listen(port, () => {
    console.log('Server actif')
});