# Présentation du projet

- Ce projet est une application de gestion de fiches de personnages de jeu de rôle papier : tel une bibliothèque du rôliste, cette application permettra de stocker les différents personnages créés par un.e joueur.euse et de consulter ces informations facilement.
- Le projet comportera également des outils utilisables durant les parties, pour améliorer l'expérience de jeu, comme des compteurs multiples ou un système de prise de notes.
- Le projet a pour objectif d'être utilisable sur mobile, avec une interface adaptée aux smartphones.
